/**
 * Ex リソース（エディットコントロール・ラジオボタンの利用）
 *
 * See_Also:
 *      https://c-lang.sevendays-study.com/win32/ex-day6.html
 *
 * License: Proprietary
 */
module exday6.WinMain;


version (Windows):

//必要なモジュールの読み込み
private static import core.sys.windows.winnt;
private static import core.sys.windows.windef;
private static import core.sys.windows.winuser;

//	リソースの読み込み
private static import exday6.resource;

/**
 * アプリケーションのエントリー関数
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		//モーダルダイアログボックスを作成
		core.sys.windows.winuser.DialogBoxW(hInstance, core.sys.windows.winuser.MAKEINTRESOURCEW(exday6.resource.IDD_DIALOG1), core.sys.windows.windef.NULL, cast(core.sys.windows.winuser.DLGPROC)(&.DlgWndProc));

		return 0;
	}

/**
 * ダイアログ用ウィンドウプロシージャ
 */
extern (Windows)
nothrow @nogc
core.sys.windows.windef.BOOL DlgWndProc(core.sys.windows.windef.HWND hWnd, core.sys.windows.windef.UINT iMsg, core.sys.windows.windef.WPARAM wParam, core.sys.windows.windef.LPARAM lParam)

	do
	{
		switch (iMsg) {
			//ダイアログボックスが生成されたとき
			case core.sys.windows.winuser.WM_INITDIALOG:
				core.sys.windows.winuser.SendMessageW(core.sys.windows.winuser.GetDlgItem(hWnd, exday6.resource.IDC_RADIO1), core.sys.windows.winuser.BM_SETCHECK, cast(core.sys.windows.windef.WPARAM)(core.sys.windows.winuser.BST_CHECKED), cast(core.sys.windows.windef.LPARAM)(0));

				return core.sys.windows.windef.TRUE;

			case core.sys.windows.winuser.WM_COMMAND:
				switch (core.sys.windows.windef.LOWORD(wParam)) {
					case core.sys.windows.winuser.IDOK:
						core.sys.windows.windef.INT iCheck = cast(core.sys.windows.windef.INT)(core.sys.windows.winuser.SendMessageW(core.sys.windows.winuser.GetDlgItem(hWnd, exday6.resource.IDC_RADIO1), core.sys.windows.winuser.BM_GETCHECK, cast(core.sys.windows.windef.WPARAM)(0), cast(core.sys.windows.windef.LPARAM)(0)));

						if (iCheck == core.sys.windows.winuser.BST_CHECKED) {
							core.sys.windows.winuser.SetWindowTextW(core.sys.windows.winuser.GetDlgItem(hWnd, exday6.resource.IDC_EDIT1), &("ラジオ１\0"w[0]));
						} else {
							core.sys.windows.winuser.SetWindowTextW(core.sys.windows.winuser.GetDlgItem(hWnd, exday6.resource.IDC_EDIT1), &("ラジオ２\0"w[0]));
						}

						break;

					case core.sys.windows.winuser.IDCANCEL:
						//モーダルダイアログボックスを破棄
						core.sys.windows.winuser.EndDialog(hWnd, 0);

						break;

					default:
						break;
				}

				return core.sys.windows.windef.TRUE;

			default:
				return core.sys.windows.windef.FALSE;
		}
	}
