/**
 * Ex メニュー・ポップアップメニュー
 *
 * See_Also:
 *      https://c-lang.sevendays-study.com/win32/ex-day3.html
 *
 * License: Proprietary
 */
module exday3.WinMain;


version (Windows):

//必要なモジュールの読み込み
private static import core.sys.windows.commdlg;
private static import core.sys.windows.shellapi;
private static import core.sys.windows.windef;
private static import core.sys.windows.winnt;
private static import core.sys.windows.winuser;

//	リソースの読み込み
private static import exday3.resource;

// シンボル定義及びマクロ
enum WINDOW_WIDTH = 800;
enum WINDOW_HEIGHT = 600;

//	インスタンス（グローバル変数）
extern (C)
__gshared
core.sys.windows.windef.HINSTANCE hInst;

extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		wstring szWindowClass = "Sample03\0"w;
		wstring szTitle = "ファイルダイアログのサンプル\0"w;

		core.sys.windows.winuser.WNDCLASSEXW wcex =
		{
			cbSize: core.sys.windows.winuser.WNDCLASSEXW.sizeof,
			style: core.sys.windows.winuser.CS_HREDRAW | core.sys.windows.winuser.CS_VREDRAW,
			lpfnWndProc: &.WndProc,
			cbClsExtra: 0,
			cbWndExtra: 0,
			hInstance: hInstance,
			hIcon: core.sys.windows.winuser.LoadIconW(hInstance, core.sys.windows.winuser.MAKEINTRESOURCEW(core.sys.windows.winuser.IDI_APPLICATION)),
			hCursor: core.sys.windows.winuser.LoadCursorW(core.sys.windows.windef.NULL, core.sys.windows.winuser.IDC_ARROW),
			hbrBackground: cast(core.sys.windows.windef.HBRUSH)(core.sys.windows.winuser.COLOR_WINDOW + 1),
			lpszMenuName: core.sys.windows.winuser.MAKEINTRESOURCEW(exday3.resource.IDR_MENU1),
			lpszClassName: &(szWindowClass[0]),
			hIconSm: core.sys.windows.winuser.LoadIconW(hInstance, core.sys.windows.winuser.MAKEINTRESOURCEW(core.sys.windows.winuser.IDI_APPLICATION)),
		};

		if (!core.sys.windows.winuser.RegisterClassExW(&wcex)) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &("RegisterClassExの処理に失敗しました\0"w[0]), &("Sample03\0"w[0]), core.sys.windows.winuser.MB_OK);

			return 1;
		}

		// グローバル変数に値を入れる
		.hInst = hInstance;

		// The parameters to CreateWindow explained:
		// szWindowClass				: アプリケーションの名前
		// szTitle						: タイトルバーに現れる文字列
		// WS_OVERLAPPEDWINDOW			: 生成するウィンドウのタイプ
		// CW_USEDEFAULT, CW_USEDEFAULT	: 最初に置くポジション (x, y)
		// WINDOW_WIDTH, WINDOW_HEIGHT	: 最初のサイズ (幅, 高さ)
		// core.sys.windows.windef.NULL							: このウィンドウの親ウィンドウのハンドル
		// core.sys.windows.windef.NULL							: メニューバー（このサンプルでは使用せず）
		// hInstance					: WinMain関数の最初のパラメータ
		// core.sys.windows.windef.NULL							: WM_CREATE情報（このアプリケーションでは使用せず）
		core.sys.windows.windef.HWND hWnd = core.sys.windows.winuser.CreateWindowW(&(szWindowClass[0]), &(szTitle[0]), core.sys.windows.winuser.WS_OVERLAPPEDWINDOW, core.sys.windows.winuser.CW_USEDEFAULT, core.sys.windows.winuser.CW_USEDEFAULT, .WINDOW_WIDTH, .WINDOW_HEIGHT, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, hInstance, core.sys.windows.windef.NULL);

		//	ウィンドウが生成できなかった場合
		if (hWnd == core.sys.windows.windef.NULL) {
			core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &("ウィンドウ生成に失敗しました!\0"w[0]), &("Sample03\0"w[0]), core.sys.windows.winuser.MB_OK);

			return 1;
		}

		// ウィンドウの表示に必要なパラメータ:
		// hWnd		: CreateWindowの戻り値
		// nCmdShow	: WinMainの引数の4番目
		core.sys.windows.winuser.ShowWindow(hWnd, nCmdShow);
		core.sys.windows.winuser.UpdateWindow(hWnd);

		// メインのメッセージループ:
		core.sys.windows.winuser.MSG msg;

		while (core.sys.windows.winuser.GetMessageW(&msg, core.sys.windows.windef.NULL, 0, 0)) {
			core.sys.windows.winuser.TranslateMessage(&msg);
			core.sys.windows.winuser.DispatchMessageW(&msg);
		}

		return cast(int)(msg.wParam);
	}

/**
 * ウィンドウプロシージャ（メッセージに対するコールバック関数）
 */
extern (Windows)
nothrow @nogc
core.sys.windows.windef.LRESULT WndProc(core.sys.windows.windef.HWND hWnd, core.sys.windows.windef.UINT message, core.sys.windows.windef.WPARAM wParam, core.sys.windows.windef.LPARAM lParam)

	do
	{
		switch (message) {
			//キーを押した
			case core.sys.windows.winuser.WM_KEYDOWN:
				switch (wParam) {
					//エスケープキー
					case core.sys.windows.winuser.VK_ESCAPE:
						//終了メッセージを発生させる
						core.sys.windows.winuser.PostMessageW(hWnd, core.sys.windows.winuser.WM_CLOSE, 0, 0);

						break;

					default:
						break;
				}

				break;

			//ウインドウが生成されたときに1度だけ通過
			case core.sys.windows.winuser.WM_CREATE:
				break;

			//マウス右クリック
			case core.sys.windows.winuser.WM_RBUTTONDOWN:
				//ポイント構造体
				core.sys.windows.windef.POINT pt =
				{
					x: core.sys.windows.windef.LOWORD(lParam),
					y: core.sys.windows.windef.HIWORD(lParam),
				};

				//クライアント座標をスクリーン座標へ変換
				core.sys.windows.winuser.ClientToScreen(hWnd, &pt);

				//ポップアップメニューを表示
				core.sys.windows.winuser.TrackPopupMenu(core.sys.windows.winuser.GetSubMenu(core.sys.windows.winuser.GetMenu(hWnd), 0), core.sys.windows.winuser.TPM_LEFTALIGN, pt.x, pt.y, 0, hWnd, core.sys.windows.windef.NULL);

				break;

			case core.sys.windows.winuser.WM_COMMAND:
				switch (core.sys.windows.windef.LOWORD(wParam)) {
					//バージョン(A)
					case exday3.resource.ID_40001:
						core.sys.windows.winuser.MessageBoxW(hWnd, &("メニューの実装Ver1.0\0"w[0]), &("バージョン情報\0"w[0]), core.sys.windows.winuser.MB_OK);

						break;

					//終了(X)
					case exday3.resource.ID_40002:
						core.sys.windows.winuser.PostMessageW(hWnd, core.sys.windows.winuser.WM_CLOSE, 0, 0);

						break;

					//開く(O)
					case exday3.resource.ID_40003:
						core.sys.windows.winnt.WCHAR[core.sys.windows.windef.MAX_PATH] szFileName = '\0';
						core.sys.windows.winnt.WCHAR[64] szFileTitle = '\0';

						core.sys.windows.commdlg.OPENFILENAMEW oFileName =
						{
							lStructSize: core.sys.windows.commdlg.OPENFILENAMEW.sizeof,
							hwndOwner: hWnd,
							lpstrFilter: &("DOC Files\0*.doc;\0All Files(*.*)\0*.*\0\0\0"w[0]),
							nFilterIndex: 1,
							lpstrFile: &(szFileName[0]),
							nMaxFile: core.sys.windows.windef.MAX_PATH,
							Flags: core.sys.windows.commdlg.OFN_FILEMUSTEXIST | core.sys.windows.commdlg.OFN_HIDEREADONLY,
							lpstrDefExt: &("\0"w[0]),
							nMaxFileTitle: 64,
							lpstrFileTitle: &(szFileTitle[0]),
							lpstrTitle: core.sys.windows.windef.NULL,
						};

						//［ファイルを開く］ダイアログボックスを作成
						core.sys.windows.commdlg.GetOpenFileNameW(&oFileName);

						//指定されたファイルに対して、指定された操作を実行
						if (oFileName.lpstrFile[0]) {
							core.sys.windows.shellapi.ShellExecuteW(hWnd, &("open\0"w[0]), oFileName.lpstrFile, core.sys.windows.windef.NULL, core.sys.windows.windef.NULL, core.sys.windows.winuser.SW_SHOWDEFAULT);
						}

						break;

					default:
						break;
				}

				break;

			case core.sys.windows.winuser.WM_DESTROY:
				core.sys.windows.winuser.PostQuitMessage(0);

				break;

			default:
				return core.sys.windows.winuser.DefWindowProcW(hWnd, message, wParam, lParam);
		}

		return 0;
	}
