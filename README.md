# 一週間で身につくWIN32プログラミングの基本
[一週間で身につくWIN32プログラミングの基本](https://c-lang.sevendays-study.com/win32/index_win32.html)のソースコードをD言語で書き直したものです。

## 実行ファイルの生成
以下のいずれかのコマンドで、実行ファイルを生成することができます。

```
dub build --arch=x86_mscoff
dub build --arch=x86_64
```

## リソースのコンパイル
応用編以降に付属されている.rcファイルはVisual Studioに付属しているrc.exeでコンパイルすることができます。
.rcファイル内でresource.hやアイコンファイルなどの読み込みが指定されていた場合、同時にコンパイルされます。

```
rc /n resource.rc
```
