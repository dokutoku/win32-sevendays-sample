/**
 * ダイアログを表示するサンプル
 *
 * See_Also:
 *      https://c-lang.sevendays-study.com/win32/day1.html
 *
 * License: Proprietary
 */
module day1.WinMain;


version (Windows):

//必要なモジュールの読み込み
private static import core.sys.windows.winnt;
private static import core.sys.windows.windef;
private static import core.sys.windows.winuser;

/**
 * アプリケーションのエントリー関数
 */
extern (Windows)
nothrow @nogc
int WinMain(core.sys.windows.windef.HINSTANCE hInstance, core.sys.windows.windef.HINSTANCE hPrevInstance, core.sys.windows.winnt.LPSTR lpCmdLine, int nCmdShow)

	do
	{
		core.sys.windows.winuser.MessageBoxW(core.sys.windows.windef.NULL, &("処理は成功しました。\0"w[0]), &("情報\0"w[0]), core.sys.windows.winuser.MB_OK);

		return 0;
	}
